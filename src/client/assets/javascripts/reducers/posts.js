import {
  GET_POSTS_LIST_SUCCESS,
  GET_POSTS_LIST_ERROR
} from '../actions/posts/types';

import _ from 'lodash';

export default function(state = {}, action) {
  switch(action.type) {
    case GET_POSTS_LIST_SUCCESS:
      return {
        postsList: _.mapKeys(action.payload.postsList.results, 'id'),
        page: action.payload.page,
        searchTerm: action.payload.searchTerm,
        hasNext: action.payload.hasNext,
      };
      
    case GET_POSTS_LIST_ERROR:
      return { ...state, error: action.payload };
      
    default:
      return state;
  }
}
