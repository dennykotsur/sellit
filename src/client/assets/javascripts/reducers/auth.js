import {
  AUTH_SIGN_UP_ERROR,
  AUTH_SIGN_IN_SUCCESS,
  AUTH_SIGN_OUT,
  AUTH_SIGN_IN_ERROR,
  AUTH_SIGN_UP_SUCCESS,
  CLEAR_AUTH_SIGNUP_MESSAGE,
  CLEAR_ERROR_MESSAGE,
  GET_USER_INFO_SUCCESS
} from '../actions/auth/types';

export default function(state = {}, action) {
    switch(action.type) {
      case AUTH_SIGN_UP_SUCCESS:
        return { ...state, signupMessage: 'Your account has been created. Use this credentials to login.' };
      
      case AUTH_SIGN_IN_SUCCESS:
        return { ...state, user: action.payload,
          authenticated: true, errors: null, signupMessage: null };
          
      case AUTH_SIGN_OUT:
        return { ...state, authenticated: false, errors: null };
          
      case AUTH_SIGN_UP_ERROR:
        return { ...state, errors: action.payload };
          
      case AUTH_SIGN_IN_ERROR:
        return { ...state, errors: action.payload, signupMessage: null };
          
      case CLEAR_ERROR_MESSAGE:
        return { ...state, errors: null };
        
      case CLEAR_AUTH_SIGNUP_MESSAGE:
        return { ...state, signupMessage: null };
        
      case GET_USER_INFO_SUCCESS:
        return { ...state, user: action.payload };
    }
    
    return state;
}
