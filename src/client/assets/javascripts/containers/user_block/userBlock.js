import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

import styles from './styles.scss';
import CSSModules from 'react-css-modules';

import { getUserInfo } from '../../actions/auth/index';

class UserBlock extends Component {
  
  componentWillMount() {
   this.props.getUserInfo();
  }
  
  displayBlock() {
    if (this.props.authenticated) {
      
      let img = 'http://www.moreradio.org/artists/content//default.jpg';
      let nickname = 'Default';
      const user = this.props.user;
      console.log(user)
      if (user) {
        if (user.photo) {
          img = user.photo.photo || 'http://www.moreradio.org/artists/content//default.jpg';
        }
        
        nickname = user.username;
      }
      
      
      
      return (
        <div styleName="user-block-wrapper">
          <div styleName="main-block">
            <div styleName="user-title-wrap">
              <img src={img} title="avatar" />
              <span> {nickname} </span>
            </div>
            <Link to="/account/logout">
              <i className="fa fa-sign-out"
                 styleName="sign-out-icon"
                 aria-hidden="true" />
            </Link>
          </div>
          <div styleName="expanded-menu">
            <ul>
              <li>
                <Link to="/posts/add"> Add New Post </Link>
              </li>
              <li>
                <Link to="/profile"> Profile </Link>
              </li>
            </ul>
          </div>
        </div>
      )
    } else {
      return (
         <span>
            Welcome,
            <Link to="/account/sign-in"> login </Link> or
            <Link to="/account/sign-up"> register </Link> for start!
         </span>
      )
    }
  }
  
  render() {
    return (
      <div styleName="wrap">
        {this.displayBlock()}
      </div>
    )
  }
}

function mapStateToProps({ auth }) {
  return {
    authenticated: auth.authenticated,
    user: auth.user
  };
}

export default connect(mapStateToProps, { getUserInfo })(CSSModules(UserBlock, styles));
