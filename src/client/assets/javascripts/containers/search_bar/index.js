import React, { Component } from 'react';
import { connect } from 'react-redux';

import _ from 'lodash';

import styles from './styles.scss';
import CSSModules from 'react-css-modules';

import { getPostsList } from '../../actions/posts/index';


class SearchComponent extends Component {
  constructor(props) {
    super(props);
    
    
    this.searchPosts = this.searchPosts.bind(this);
    this.getPostsListsDeb = _.debounce((term, page) => {this.props.getPostsList(page, null, term)}, 300);
  }
  
  
  searchPosts(e) {
    const page = this.props.page;
    this.getPostsListsDeb(e.target.value, page);
  }
  
  render() {
    return (
      <div className={styles.searchWrapper}>
        <i className="fa fa-search" />
        <input
          type="text"
          placeholder="Try to find something"
          className={styles.searchInput}
          onChange={this.searchPosts}
        />
      </div>
    )
  }
}

function mapStateToProps({ posts }) {
  return {
    page: posts.page,
    searchTerm: posts.searchTerm
  }
}

export default connect(mapStateToProps, { getPostsList })(CSSModules(SearchComponent, styles));
