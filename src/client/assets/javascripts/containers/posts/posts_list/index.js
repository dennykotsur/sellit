import React, { Component } from 'react';
import { connect } from 'react-redux';

import _ from 'lodash';
import InfiniteScroll from 'react-infinite-scroller';

import styles from './styles.scss';
import CSSModules from 'react-css-modules';

import { getPostsList } from '../../../actions/posts/index';
  
import PostItem from '../../../components/posts/post_item/index';


class PostsList extends Component {
  componentDidMount() {
    this.props.getPostsList(1, null, this.props.searchTerm || '');
  }
  
  loadMore() {
    const page = this.props.page + 1;
    this.props.getPostsList(page, null, this.props.searchTerm || '')
  }
  
  renderItems() {
    return _.map(this.props.posts, post => {
      const photo = post.photo_details.length && post.photo_details[0].photo;
      return (
        <PostItem
          key={post.id}
          title={post.title}
          srcImg={photo}
        />
      )
    })
  }
  
  render() {
    return (
      <div styleName="posts-list-wrapper">
        <InfiniteScroll
          pageStart={1}
          loadMore={this.loadMore.bind(this)}
          hasMore={this.props.hasNext}
          threshold={400}
          initialLoad={false}
        >
          {this.renderItems()}
        </InfiniteScroll>
      </div>
    )
  }
}


function mapStateToProps({ posts }) {
  return {
    posts: posts.postsList,
    page: posts.page,
    searchTerm: posts.searchTerm,
    hasNext: posts.hasNext
  }
}

export default connect(mapStateToProps, { getPostsList })(CSSModules(PostsList, styles));
