import React, { Component } from 'react';
import { connect } from 'react-redux';

import { signOut } from '../../actions/auth/index';


class SignOut extends Component {
  componentWillMount() {
    this.props.signOut();
    this.props.history.push('/account/sign-in');
  }
  
  render() {
    return null;
  }
}

export default connect(null, { signOut })(SignOut);
