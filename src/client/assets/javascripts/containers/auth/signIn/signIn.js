import React, { Component } from 'react';
import { Field, reduxForm } from 'redux-form';
import { connect } from 'react-redux';

import AuthField from '../../../components/core/inputs/auth/field';
import AuthButton from '../../../components/core/buttons/auth/button';

import * as validations from '../../../utils/validations';
import { signIn, clearErrorMessage, clearSignupMessage } from '../../../actions/auth/index';

import styles from './styles.scss';
import { mapErrors } from '../../../utils/util';



class SignUp extends Component {
    constructor(props) {
        super(props);
        
        this.onSubmit = this.onSubmit.bind(this);
        this.successSignIn = this.successSignIn.bind(this);
    }
    
    componentWillUnmount() {
      this.props.clearErrorMessage();
      this.props.clearSignupMessage();
    }
    
    renderField({input, meta: {touched, error}, ...params}) {
        return (
            <AuthField
                errorText={touched && error}
                inputProps={input}
                {...params}
            />
        )
    }
    successSignIn() {
        this.props.history.push('/');
    }
    
    onSubmit(values) {
        this.props.signIn(values, this.successSignIn)
    }
    
    renderErrors() {
      const { errors } = this.props;
      if (errors !== null) {
        return mapErrors(errors);
      }
    }
    
    render() {
        const { handleSubmit } = this.props;
        return (
            <form onSubmit={handleSubmit(this.onSubmit.bind(this))}>
                <div className={styles.signupMessage}>
                  {this.props.signupMessage}
                </div>
                <div className={styles.fieldsWrap}>
                  <Field
                      name="email"
                      placeholder="Email"
                      type="text"
                      validate={[
                          validations.required,
                          validations.email
                      ]}
                      component={this.renderField}
                  />
                  <Field
                      name="password"
                      placeholder="Password"
                      type="password"
                      validate={validations.required}
                      component={this.renderField}
                  />
                </div>
                <div className={styles.errors}>
                  {this.renderErrors()}
                </div>
                <AuthButton
                    text="Login"
                />
            </form>
        )
    }
}

function mapStateToProps({ auth }) {
  return {errors: auth.errors, signupMessage: auth.signupMessage}
}

export default reduxForm({
    form: 'signIn',
})(
    connect(mapStateToProps,
      { signIn, clearErrorMessage, clearSignupMessage }
    )(SignUp)
);
