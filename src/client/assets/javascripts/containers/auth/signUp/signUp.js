import React, { Component } from 'react';
import { Field, reduxForm } from 'redux-form';
import { connect }from 'react-redux';
import AuthField from '../../../components/core/inputs/auth/field';
import AuthButton from '../../../components/core/buttons/auth/button';

import * as validations from '../../../utils/validations';
import { signUp, clearErrorMessage } from '../../../actions/auth/index';

import styles from './styles.scss';
import { mapErrors } from '../../../utils/util';


class SignUp extends Component {
    constructor(props) {
        super(props);
        
        this.onSubmit = this.onSubmit.bind(this);
        this.successSignUp = this.successSignUp.bind(this);
    }
    
    componentWillUnmount() {
      this.props.clearErrorMessage();
    }
    
    renderField({input, meta: {touched, error}, ...params}) {
        return (
            <AuthField
                errorText={touched && error}
                inputProps={input}
                {...params}
            />
        )
    }
    
    successSignUp() {
      this.props.history.push('/account/sign-in');
    }
    
    onSubmit(values) {
        this.props.signUp(values, this.successSignUp);
    }
    
    renderErrors() {
      const { errors } = this.props;
      if (errors) {
        return mapErrors(errors);
      }
    }
    
    render() {
        const { handleSubmit } = this.props;
        
        return (
            <form onSubmit={handleSubmit(this.onSubmit.bind(this))}>
              <div className={styles.fieldsWrap}>
                <Field
                    name="username"
                    placeholder="Username"
                    type="text"
                    validate={[
                        validations.required,
                        validations.maxLength15
                    ]}
                    component={this.renderField}
                />
                <Field
                    name="email"
                    placeholder="Email"
                    type="text"
                    validate={[
                        validations.required,
                        validations.email
                    ]}
                    component={this.renderField}
                />
                <Field
                    name="password"
                    placeholder="Password"
                    type="password"
                    validate={[
                        validations.required
                    ]}
                    component={this.renderField}
                />
                <Field
                    name="password_confirm"
                    type="password"
                    placeholder="Confirm password"
                    validate={[
                        validations.required
                    ]}
                    component={this.renderField}
                />
              </div>
              <div className={styles.errors}>
                {this.renderErrors()}
              </div>
              <AuthButton
                  text="Register"
              />
            </form>
        )
    }
}

const validate = values => {
  const errors = {};
  
  if (values.password != values.password_confirm) {
      errors.password_confirm = "Your passwords don't match.";
  }
  
  return errors;
};

function mapStateToProps({ auth }) {
  return {errors: auth.errors}
}

export default reduxForm({
    validate,
    form: 'signUp',
})(
    connect(mapStateToProps, { signUp, clearErrorMessage })(SignUp)
);
