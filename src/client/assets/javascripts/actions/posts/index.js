import {
  GET_POSTS_LIST_SUCCESS,
  GET_POSTS_LIST_ERROR
} from './types';

import axios from 'axios';
import { ROOT_URL, ITEMS_PER_PAGE } from '../../app.config';


export function getPostsList(page=1, offset=null, search=null) {
  const limit = page * ITEMS_PER_PAGE;
  return function(dispatch) {
    axios.get(`${ROOT_URL}poster/`, { params: { limit, offset, search } })
      .then(resp => {
        const next = !!resp.data.next;
        dispatch(getPostsListSuccess(resp.data, search, page, next));
      })
      .catch(error => {
        dispatch(getPostsListError(error));
      })
  }
}


export function getPostsListSuccess(posts, searchTerm, page, hasNext) {
  return {
    type: GET_POSTS_LIST_SUCCESS,
    payload: {
      postsList: posts,
      searchTerm: searchTerm,
      page: page,
      hasNext: hasNext
    }
  }
}

export function getPostsListError(error) {
  return {
    type: GET_POSTS_LIST_ERROR,
    payload: error
  }
}
