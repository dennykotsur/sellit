export const GET_POSTS_LIST = 'get_posts_list';
export const GET_POSTS_LIST_SUCCESS = 'get_posts_list_success';
export const GET_POSTS_LIST_ERROR = 'get_posts_list_error';
