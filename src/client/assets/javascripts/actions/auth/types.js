/* AUTH */
export const AUTH_SIGN_UP = 'auth_sign_up';
export const AUTH_SIGN_UP_SUCCESS = 'auth_sign_up_success';
export const AUTH_SIGN_UP_ERROR = 'auth_sign_up_error';

export const AUTH_SIGN_IN = 'auth_sign_in';
export const AUTH_SIGN_IN_SUCCESS = 'auth_sign_in_success';
export const AUTH_SIGN_IN_ERROR = 'auth_sign_in_error';


export const CLEAR_ERROR_MESSAGE = 'clear_error_message';
export const CLEAR_AUTH_SIGNUP_MESSAGE = 'clear_signup_message';
export const AUTH_SIGN_OUT = 'auth_sign_out';

/* USER */
export const GET_USER_INFO = 'get_user_info';
export const GET_USER_INFO_SUCCESS = 'get_user_info_success';



