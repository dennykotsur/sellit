import {
  AUTH_SIGN_UP_SUCCESS,
  AUTH_SIGN_IN,
  AUTH_SIGN_IN_SUCCESS,
  AUTH_SIGN_OUT,
  AUTH_SIGN_IN_ERROR,
  AUTH_SIGN_UP_ERROR,
  CLEAR_ERROR_MESSAGE,
  CLEAR_AUTH_SIGNUP_MESSAGE,
  GET_USER_INFO_SUCCESS,
  GET_POSTS_LIST,
} from './types';
import axios from 'axios';
import { ROOT_URL } from '../../app.config';


/* SIGN UP */
export function signUp(data, callback) {
    return function (dispatch) {
        axios.post(`${ROOT_URL}signup/`, { ...data })
            .then(function(response) {
                // Adding some crutches because of bad response from server
                // It returns 200 instead of 400 on an error
                if (response.data) {
                  if (response.data.error) {
                    dispatch(signUpError(response.data))
                  } else {
                    if (response.data.success === undefined) {
                      dispatch(signUpError(response.data))
                    } else {
                      dispatch({type: AUTH_SIGN_UP_SUCCESS});
                      if (callback) {
                          callback();
                      }
                    }
                  }
                }
            })
            .catch(error => {
              dispatch(signUpError(error.response.data))
            });
    }
}

export function signUpError(error) {
  return {
    type: AUTH_SIGN_UP_ERROR,
    payload: error
  }
}

/* SIGN IN */
export function signIn({ email, password }, callback) {
    return function (dispatch) {
        axios.post(`${ROOT_URL}login/`, { email, password })
          .then(response => {
              dispatch({type: AUTH_SIGN_IN_SUCCESS});
              localStorage.setItem('token', response.data.token);
              
              if (callback) {
                  callback();
              }
          })
          .catch(error => {
              dispatch(signInError(error.response.data))
          })
    }
}

export function signInSuccess(userData) {
  return {
    type: AUTH_SIGN_IN_SUCCESS,
    payload: userData
  }
}

export function signInError(error) {
  return {
    type: AUTH_SIGN_IN_ERROR,
    payload: error
  }
}

/* SIGN OUT */
export function signOut() {
  return function (dispatch) {
    axios.post(`${ROOT_URL}logout/`)
      .then(response => {
        dispatch({type: AUTH_SIGN_OUT});
        localStorage.removeItem('token');
      })
  }
}


export function clearErrorMessage() {
  return {
    type: CLEAR_ERROR_MESSAGE
  }
}

export function clearSignupMessage() {
  return {
    type: CLEAR_AUTH_SIGNUP_MESSAGE
  }
}


/* USER */
export function getUserInfo(callback) {
  return function(dispatch) {
     axios.get(`${ROOT_URL}profile/me/`, {
       headers: { Authorization: `Token ${localStorage.getItem('token')}` }
      })
      .then(resp => {
        dispatch(getUserInfoSuccess(resp.data));
      })
      .catch(() => {
        if (callback) {
          callback();
        }
      })
  }
}

export function getUserInfoSuccess(userInfo) {
  return {
    type: GET_USER_INFO_SUCCESS,
    payload: userInfo
  }
}
