import React, { Component } from 'react';
import Header from '../../components/header/header';
import Footer from '../../components/footer/index';
import CSSModules from 'react-css-modules';
import styles from './style.scss';

import PostsList from '../../containers/posts/posts_list/index';

class HomePage extends Component {
  render() {
    return (
      <div>
        <Header />
        <PostsList />
        <Footer />
      </div>
    );
  }
}

export default CSSModules(HomePage, styles)
