import React, { Component } from 'react';
import { BrowserRouter as Router, Route, NavLink } from 'react-router-dom';
import DefaultButton from '../../components/core/buttons/default/button';

import SignIn from '../../containers/auth/signIn/signIn';
import SignUp from '../../containers/auth/signUp/signUp';

import styles from './styles.scss';
import CSSModules from 'react-css-modules';


class AuthPage extends Component {
    render() {
        return (
            <div styleName='wrapper'>
                <div styleName="left-section"></div>
                <div styleName="right-section">
                    <div styleName="logo">
                    </div>
                    <div styleName="auth-buttons">
                        <NavLink
                          to="/account/sign-in"
                          activeClassName={styles.authBtnActive}
                        >
                            <DefaultButton name="Sign In"/>
                        </NavLink>
                        
                        <NavLink
                          to="/account/sign-up"
                          activeClassName={styles.authBtnActive}
                        >
                            <DefaultButton name="Sign Up"/>
                        </NavLink>
                    </div>
                    <div styleName="auth-footer">
                      <span> Frontend labs 2017 </span>
                    </div>
                    
                    <div>
                        <Route path="/account/sign-in" component={SignIn} />
                        <Route path="/account/sign-up" component={SignUp} />
                    </div>
                </div>
            </div>
        )
    }
}

export default CSSModules(AuthPage, styles);
