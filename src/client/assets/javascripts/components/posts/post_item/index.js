import React, { Component } from 'react';
import { connect } from 'react-redux';

import styles from './styles.scss';
import CSSModules from 'react-css-modules';


class PostItem extends Component {
  
   render() {
     const imgSrc = this.props.srcImg || 'http://goldwallpapers.com/uploads/posts/high-resolution-animal-wallpapers/high_resolution_animal_wallpapers_012.jpg';
     const title = this.props.title || 'Default Title';
     
     return (
       <div styleName="item-wrapper">
         <div styleName="item-image">
           <img src={imgSrc} />
         </div>
         <div styleName="item-title">
           <span> {title} </span>
           <i className="fa fa-eye" aria-hidden="true" />
         </div>
       </div>
     )
   }
}


export default CSSModules(PostItem, styles);
