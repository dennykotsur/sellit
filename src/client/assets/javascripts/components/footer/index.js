import React, { Component } from 'react';
import styles from './styles.scss';
import CSSModules from 'react-css-modules';


class Footer extends Component {
  render() {
    return (
      <div styleName="footer">
        <span>
          2017 - front-end labs Light IT
        </span>
      </div>
    )
  }
}

export default CSSModules(Footer, styles);
