import React, { Component } from 'react';
import { Link } from 'react-router-dom';

import styles from './styles.scss';
import CSSModules from 'react-css-modules';

import UserBlock from '../../containers/user_block/userBlock';
import SearchBar from '../../containers/search_bar/index';

class Header extends Component {
  render() {
    return (
      <div styleName="header-wrapper">
        <div styleName="logo">
          <Link to="/">
            <img src="/images/sell-it-blue.png" alt="Logo" />
          </Link>
        </div>
        
        <div styleName="search-wrapper">
          <SearchBar />
        </div>
        
        <div styleName="user-block-wrapper">
          <UserBlock />
        </div>
      </div>
    )
  }
}

export default CSSModules(Header, styles);
