import React, { Component } from 'react';
import CSSModules from 'react-css-modules';
import styles from './styles.scss';

class Field extends Component {

    render() {
        const {errorText, name, type, placeholder } = this.props;
        const styleClass = errorText ? 'auth-input-error' : 'auth-input-main';
        return (
            <div>
                <input
                    name={name}
                    type={type || 'text'}
                    placeholder={placeholder}
                    styleName={styleClass}
                    {...this.props.inputProps}
                />
                <div styleName="error-text">{errorText ? errorText : ''}</div>
            </div>
        )
    }
}

export default CSSModules(Field, styles)
