import React, { Component } from 'react';
import CSSModules from 'react-css-modules';
import styles from './styles.scss';


class DefaultButton extends Component {
    render() {
        return (
            <button styleName='auth-btn-default'>
                {this.props.name}
            </button>
        )
    }
}

export default CSSModules(DefaultButton, styles);
