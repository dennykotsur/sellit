import React, { Component } from 'react';
import styles from './styles.scss';

export default class AuthButton extends Component {
    render() {
        return (
            <input type="submit"
                   className={styles.main}
                   onClick={this.props.onClickFunc || null}
                   value={this.props.text || 'Auth Button'}
            >
            </input>
        )
    }
}
