import React from 'react';
import { createStore, applyMiddleware, compose } from 'redux';
import { Provider } from 'react-redux';
import ReactDOM from 'react-dom';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import reduxThunk from 'redux-thunk';
import reducers from './reducers';

import { PrivateRoute } from './components/auth/requireAuthentication';

import AuthPage from './pages/auth/authPage';
import HomePage from './pages/home/homePage';
import SignOut from './containers/auth/signOut';

import { AUTH_SIGN_IN_SUCCESS } from './actions/auth/types';

const createStoreWithMiddleware = compose(
    applyMiddleware(reduxThunk),
    window.devToolsExtension ? window.devToolsExtension() : f => f
)(createStore);
    
const store = createStoreWithMiddleware(reducers);

const token = localStorage.getItem('token');

if (token) {
  store.dispatch({ type: AUTH_SIGN_IN_SUCCESS })
}


ReactDOM.render(
  <Provider store={store}>
      <BrowserRouter>
          <div>
              <Switch>
                  <Route path="/account/logout" component={SignOut} />
                  <Route path="/account" component={AuthPage} />
                  <Route exact path="/" component={HomePage} />
              </Switch>
          </div>
      </BrowserRouter>
  </Provider>
  , document.querySelector('#app'));
