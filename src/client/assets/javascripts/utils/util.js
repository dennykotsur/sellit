export const mapErrors = errors => {
  let errorsArray = [];
  for (let key in errors) {
    if (errors.hasOwnProperty(key)) {
      if (typeof errors[key] !== "string") {
        errors[key].forEach(item => {
          errorsArray.push(item);
        });
      } else {
        errorsArray.push(errors[key])
      }
    }
  }
  return errorsArray;
};
